# Registration service API V1 (Metadata test)

### Run 

```bash
docker-compose up --force-recreate --build -d
```
- note: you may omit the '-d' to watch the logs 

For non docker setting
```bash
mvn spring-boot:run
```

To enable debug mode 
```bash
mvn spring-boot:run -Dspring-boot.run.arguments=--logging.level.org.springframework=TRACE
```

To specify build/run environment (dev, prod or staging...etc)
```bash
mvn spring-boot:run -Dspring.profiles.active=dev 
```

## Shutdown
```bash
docker-compose down
```

## Admin (to manually query db)

To find the mysql container id, whose image name is mysql:latest
```bash
docker ps -a 
```
To access mysql container 

```bash 
docker exec -it db_container_id /bin/sh
```

Once in the (mysql) container, for credentials see: .env file 
Adjust to your liking for env to env
```bash
mysql -u root -p  
```

## API documentation 
after server startup, open browser: http://localhost:8080/apidocs/

## Test
- install [postman](https://www.postman.com/downloads/)

- follow the [docs](https://learning.postman.com/docs/getting-started/introduction/)

## TODO
implement better error/exception handling